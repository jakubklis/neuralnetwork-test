package test.test;

import org.junit.Test;

import neuralNet.CsvReader;
import neuralNet.NeuralNet;
import neuralNet.data.ModelData;

public class AppTest {

	@Test
    public void createNeuralNet () {
    	
    	CsvReader csv = new CsvReader();
    	double [][] input = csv.readInput();
    	
    	int rowLength = input[0].length;
    	int testLength = 10;
    	int learnLength = input.length-testLength;
    	
    
    	
    	double [][][] inputLearn = new double [learnLength-2][][];
    	double [] outputLearn = new double [learnLength-2];
    	
    	double [][][] inputTest = new double [testLength][][];
    	double [] outputTest= new double [testLength];
    			
    	int y =0;
    	for (int i =2; i<learnLength;i++) {
    		outputLearn[y] = input[i][rowLength-1]; 
    		inputLearn[y] = new double [][] {input[i],input[i-1],input[i-2]};
    		y++;
    	}
    	
    	y =0;
    	for (int i =learnLength; i<learnLength+testLength;i++) {
    		outputTest[y] = input[i][rowLength-1]; 
    		inputTest[y] = new double [][] {input[i],input[i-1],input[i-2]};
    		y++;
    	}
    	
    	ModelData model = new ModelData(new double [0], outputLearn, outputTest, new double [0][][], inputLearn, inputTest, null, null);
    	
    	NeuralNet net = new NeuralNet(model, 10);
    	
    	net.setMaxIteration(100);
    	net.setMaxError(0.00000000001);
    	
    	net.train();
    	
    	
    }
}
