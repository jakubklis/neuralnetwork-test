package neuralNet;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class CsvReader {

	public double[][] readInput() {

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream test = loader.getResourceAsStream("test.csv");
		BufferedReader br = null;
		List<double[]> linesList = new ArrayList<>();
		try {
			br = new BufferedReader(new InputStreamReader(test));
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(br);
			for (CSVRecord record : records) {
				int size = record.size();
				double[] row = new double[size]; // first column (date) is omitted
				for (int i = 0; i < size; i++) {
					row[i] = Double.parseDouble(record.get(i).replaceAll(",", "."));
				}
				linesList.add(row);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return linesList.toArray(new double[linesList.size()][]);

	}
}
