package neuralNet.listeners;

import org.neuroph.core.Layer;
import org.neuroph.core.Neuron;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.SupervisedLearning;

public class AdditiveNeuronListener implements LearningEventListener {

	private int maxNeurons;

	public AdditiveNeuronListener(int maxNeurons) {
		this.maxNeurons = maxNeurons;
	}

	public void handleLearningEvent(LearningEvent learningEvent) {
		System.out.println("AdditiveNeuronListener");
		if (learningEvent.getEventType() == LearningEvent.Type.LEARNING_STOPPED) {
			SupervisedLearning rule = (SupervisedLearning) learningEvent.getSource();
			Layer hiddenLayer = rule.getNeuralNetwork().getLayerAt(1);
			int neuronCount = hiddenLayer.getNeuronsCount();

			if (rule.getTotalNetworkError() >= rule.getMaxError()) {
				if (neuronCount <= maxNeurons) {
					addNeuron(hiddenLayer);
					rule.learn(rule.getTrainingSet());
				}
			}
		}
	}
	
	public void addNeuron (Layer layer) {
		Neuron neuron = layer.getNeuronAt(0);
		Neuron newNeuron = new Neuron (neuron.getInputFunction(), neuron.getTransferFunction());
		layer.addNeuron(newNeuron);
	}
	
}
