package neuralNet.listeners;

import org.neuroph.core.Layer;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.SupervisedLearning;

public class SubtractiveNeuronListener implements LearningEventListener {
	
	private int minNeurons;
	private int maxNeurons;
	
	
	public SubtractiveNeuronListener(int minNeurons, int maxNeurons) {
		this.minNeurons = minNeurons;
		this.maxNeurons = maxNeurons;
	}
	
		public void handleLearningEvent(LearningEvent learningEvent) {
			if (learningEvent.getEventType()==LearningEvent.Type.LEARNING_STOPPED) {
				SupervisedLearning rule = (SupervisedLearning) learningEvent.getSource();
				Layer hiddenLayer = rule.getNeuralNetwork().getLayerAt(1);
				int neuronCount = hiddenLayer.getNeuronsCount();
				if (rule.getTotalNetworkError()<=rule.getMaxError()) {
					if (neuronCount>minNeurons) {
						hiddenLayer.removeNeuronAt(neuronCount-1);
						rule.learn(rule.getTrainingSet());
					}
				}
			}
		}
		
}
