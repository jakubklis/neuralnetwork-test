package neuralNet.data;

public class ModelData {
	
	public double[] fx;
	public double[] fxLearn;
	public double[] fxTest;
	public double w[][][];
	public double wLearn[][][];
	public double wTest[][][];
	public Long[] dates;
	public Long[] series;
	private double min = Double.MAX_VALUE;
	private double max = Double.MIN_VALUE;
	
	public ModelData() {
	}

	public ModelData(double[] fx,double[] fxLearn,double[] fxTest, double[][][] w, double[][][] wLearn, double[][][] wTest,  Long[] dates, Long[] series) {
		findMinAndMax(wLearn, fxLearn);
		
		normalizeArray(fx);
		normalizeArray(fxLearn);
		normalizeArray(fxTest);
		normalizeMultidimensionalArray(w);
		normalizeMultidimensionalArray(wLearn);
		normalizeMultidimensionalArray(wTest);
		
		this.fx = fx;
		this.fxLearn = fxLearn;
		this.fxTest = fxTest;
		this.w = w;
		this.wLearn = wLearn;
		this.wTest = wTest;
		this.dates = dates;
		this.series = series;
	}
	
	private void findMinAndMax (double [][][] input, double [] output) {
		for (double d : output) {
			replaceMinAndMax(d);
		}

		for (double [][] arrf : input) {
			for (double [] arrs : arrf) {
				for (double d : arrs) {
					replaceMinAndMax(d);
				}
			}
		}
	}
	
	private void replaceMinAndMax(double value) {
		if (value > max) max = value;
		if (value<min) min = value;
	}
	
	public double normalizeValue(double input) {
		return (input - min) / (max - min) * 0.8 + 0.1;
	}

	public double deNormalizeValue(double input) {
		return min + (input - 0.1) * (max - min) / 0.8;
	}
	
	public void normalizeArray(double[] input ) {
		for (int i=0; i<input.length; i++) {
			input[i] = normalizeValue(input[i]);
		}
	}

	public void normalizeMultidimensionalArray(double[][][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int y = 0; y < arr[i].length; y++) {
				for (int z = 0; z<arr[i][y].length; z++) {
					arr[i][y][z] = normalizeValue(arr[i][y][z]);
				}
			}
		}
	}
	
}




