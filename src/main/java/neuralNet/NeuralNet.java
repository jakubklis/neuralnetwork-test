package neuralNet;

import java.util.LinkedList;
import java.util.List;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.learning.SupervisedLearning;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.TransferFunctionType;

import neuralNet.data.ModelData;

/**
 * Wrapper for  neuroph studio's MultiLayerPercepton neural network.
 *
 */
public class NeuralNet {
	
	private NeuralNetwork<BackPropagation> network;
	private ModelData model;
	private int inputLength;
	private int outputLength;
	private boolean trained;
	
	/**
	 * @param model object containing input and output data in the form of arrays of doubles
	 * @param hiddenLayers array of integers corresponding to hidden layers of a neural network
	 * 
	 * @throws IllegalArgumentException if arrays of data of the model parameter are invalid
	 */
	public NeuralNet (ModelData model, int ...hiddenLayers ) {
		this.model = model;
		this.outputLength = 1;
		
		testParameters();
		int [] networkLayers = getNetworkLayers(hiddenLayers);
		
		network = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, networkLayers);
		setNetworkListeners();
	}
	
	public NeuralNet (ModelData model) {
		this.model = model;
		this.outputLength = 1;
		
		testParameters();
		int [] networkLayers = getNetworkLayers(getNumberOfHiddenNeurons(this.inputLength, this.outputLength));
		
		network = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, networkLayers);
		setNetworkListeners();
	}

	/**
	 * Trains neural net. Repeating the method without changing ModelData
	 *  will return in data leakage which in result will make return value unreliable.
	 *
	 * @return the mean of all errors in test data set.
	 * 
	 */
	public double train() {

		trained = true;
		
		DataSet trainingSet = new DataSet(inputLength, outputLength);

		DataSet testSet = new DataSet(inputLength, outputLength);

		for (int i = 0; i < model.wLearn.length; i++) {
			double [] input = flattenAndTrim(model.wLearn[i]);
			trainingSet.addRow(new DataSetRow(input, new double [] {model.fxLearn[i]}));
		}
		
		for (int i = 0; i < model.wTest.length; i++) {
			double [] input = flattenAndTrim(model.wTest[i]);
			testSet.addRow(new DataSetRow(input, new double [] {model.fxTest[i]}));
		}
		
		network.learn(trainingSet);

		List<Double>testError = new LinkedList<>();
		for (DataSetRow testElement : testSet.getRows()) {
			network.setInput(testElement.getInput());
			network.calculate();
			double[] networkOutput = network.getOutput();
			double[] desiredNetworkOutput = testElement.getDesiredOutput();
			
			for (int i = 0; i < networkOutput.length; i++) {
				double output = model.deNormalizeValue(networkOutput[i]);
				double desiredOutput = model.deNormalizeValue(desiredNetworkOutput[i]);
				System.out.println("Output: " + output + 
						"\nDesiredOutput: " + desiredOutput);
				testError.add(getAbsoluteValue(output/desiredOutput -  1));
			}
		}
		double totalTestError = testError.stream().mapToDouble(f -> f).sum() / testError.size();
		System.out.println("\nMeanError: " + totalTestError);
		return totalTestError;
	}
	
	/**
	 * Calculate prognostic data based on input. 
	 * @return the array of prognostic data
	 * @throws IllegalArgumentException if input data is of wrong length
	 */
	public double [] calculate(double[][][] input) {
		
		inputValidation(input);
		
		if (trained==false) System.out.println("Warning : the current data model has not been trained yet");
		
		double [][] dataArray = new double [input.length][];
		for (int i = 0; i < model.wLearn.length; i++) {
			double [] data = flattenAndTrim(model.wLearn[i]);
			dataArray[i] = data;
		}
		
		double  [][] output = new double [dataArray.length][];
		for (int i =0; i< dataArray.length; i++) {
		network.setInput(dataArray[i]);
		network.calculate();
		output[i] = network.getOutput();
		}
		
		return flattenAndTrim(output);
	}
	
	public SupervisedLearning getLearningRule () {
		return network.getLearningRule();
	}
	public void setMaxError (double maxError) {
		getLearningRule().setMaxError(maxError);
	}
	
	public void setLearningRate (double learningRate) {
		getLearningRule().setLearningRate(learningRate);
	}
	
	public void setMaxIteration (int maxIterations) {
		getLearningRule().setMaxIterations(maxIterations);
	}
	
	public void setModelData (ModelData model) {
		this.model = model;
		trained = false;
	}
	
	
	
	private double getAbsoluteValue (double value) {
		return value<0 ? -value : value;
	}

	private int checkLengthOfThreeDimensionalArray(double[][][] dataSet) {
		int firstSize = 0;
		for (int i = 0; i < dataSet.length; i++) {
			int inputSize = 0;
			for (int y = 0; y < dataSet[i].length; y++) {
				inputSize += dataSet[i][y].length - 1; 			// First column is ignored
			}
			if (i==0) {
				firstSize = inputSize; 
					}
			else if(inputSize!=firstSize){
				throw new IllegalArgumentException("combined datasets are not of equal size");
			}
		}
		return firstSize;
	}
	
	private double[] flattenAndTrim(double[][] arr) {
		int length = (arr[0].length-1) * arr.length;
		double [] output = new double[length];
		for (int i=0; i<arr.length; i++) {
			double [] narr = arr[i];
			System.arraycopy(narr, 1, output, (arr[i].length-1)*i, narr.length-1);
		}
		return output;
	}
	
	private void setNetworkListeners () {
//		getLearningRule().addListener(new SubtractiveNeuronListener(minNeurons, maxNeurons));				// its subtractiveNeuronListener which later can pass work to AdditiveNL - misleading name
		getLearningRule().addListener(new LearningEventListener() {
			public void handleLearningEvent(LearningEvent learningEvent) {
				SupervisedLearning rule = (SupervisedLearning) learningEvent.getSource();
				System.out.println("Network error for interation " + rule.getCurrentIteration() + ": "
						+ rule.getTotalNetworkError());
			}});
	}

	private static int getNumberOfHiddenNeurons(int inputNeurons, int outputNeurons) {
		return (outputNeurons + inputNeurons) /2;
	}
	
	private void inputValidation (double [][][] input) {
		int size = checkLengthOfThreeDimensionalArray(input);
		if (size!=outputLength) throw new IllegalArgumentException("input data is of incorrect length for the neural network");
	}
	
	private void testParameters () {
		int learnSetLength = checkLengthOfThreeDimensionalArray(model.wLearn);
		int testSetLength = checkLengthOfThreeDimensionalArray(model.wTest);
		
		this.inputLength = learnSetLength;						
		
		if (learnSetLength!=testSetLength) throw new IllegalArgumentException("Input and output sets have different length");
		if (model.wLearn.length != model.fxLearn.length)  throw new IllegalArgumentException("Lengths of input learn array "
				+ "and corresponding output array are not equal");
		if (model.wTest.length != model.fxTest.length)  throw new IllegalArgumentException("Lengths of input test array "
				+ "and corresponding output array are not equal");
	}
	
	private int [] getNetworkLayers (int... hiddenLayers) {
		LinkedList<Integer>layersList = new LinkedList<>();
		for (int layer : hiddenLayers) {
			layersList.add(layer);
		}
		layersList.addFirst(inputLength);
		layersList.addLast(outputLength);
		return layersList.stream().mapToInt(i -> i).toArray();
	}
	
}
